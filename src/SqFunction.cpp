#include <bass.h>

SQInteger sq_bass_stream_create_file(HSQUIRRELVM vm)
{
	const SQChar *sFile;
	SQInteger iStart;
	SQInteger iEnd;
	SQInteger flags;

	sq_getstring(vm, 2, &sFile);
	sq_getinteger(vm, 3, &iStart);
	sq_getinteger(vm, 4, &iEnd);
	sq_getinteger(vm, 5, &flags);

	SQInteger stream = BASS_StreamCreateFile(false, sFile, iStart, iEnd, flags);
	sq_pushinteger(vm, stream);

	return 1;
}

SQInteger sq_bass_stream_create_url(HSQUIRRELVM vm)
{
	const SQChar *sURL;
	SQInteger offset;
	SQInteger flags;

	sq_getstring(vm, 2, &sURL);
	sq_getinteger(vm, 3, &offset);
	sq_getinteger(vm, 4, &flags);

	SQInteger stream = BASS_StreamCreateURL(sURL, offset, flags, 0, 0);
	sq_pushinteger(vm, stream);

	return 1;
}

SQInteger sq_bass_stream_free(HSQUIRRELVM vm)
{
	SQInteger iStream;

	sq_getinteger(vm, 2, &iStream);

	if (BASS_StreamFree(iStream))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}

SQInteger sq_bass_get_volume(HSQUIRRELVM vm)
{
	SQInteger iStream;

	sq_getinteger(vm, 2, &iStream);

	float fVol = 0.f;
	BASS_ChannelGetAttribute(iStream, BASS_ATTRIB_VOL, &fVol);

	SQFloat rVol = (float)fVol;
	rVol *= 100;
	sq_pushinteger(vm, (int)rVol);

	return 1;
}

SQInteger sq_bass_set_volume(HSQUIRRELVM vm)
{
	SQInteger iStream;
	SQInteger iVol;

	sq_getinteger(vm, 2, &iStream);
	sq_getinteger(vm, 3, &iVol);

	float fVolume = (float(iVol) / 100);

	if (BASS_ChannelSetAttribute(iStream, BASS_ATTRIB_VOL, fVolume))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}

SQInteger sq_bass_channel_stop(HSQUIRRELVM vm)
{
	SQInteger iStream;

	sq_getinteger(vm, 2, &iStream);

	if (BASS_ChannelStop(iStream))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}

SQInteger sq_bass_channel_play(HSQUIRRELVM vm)
{
	SQInteger iStream;
	SQBool bRestart;

	sq_getinteger(vm, 2, &iStream);
	sq_getbool(vm, 3, &bRestart);

	if (BASS_ChannelPlay(iStream, bRestart))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}

SQInteger sq_bass_channel_pause(HSQUIRRELVM vm)
{
	SQInteger iStream;

	sq_getinteger(vm, 2, &iStream);

	if (BASS_ChannelPause(iStream))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}

SQInteger sq_bass_channel_get_tags(HSQUIRRELVM vm)
{
	SQInteger iStream;
	SQInteger iTag;

	sq_getinteger(vm, 2, &iStream);
	sq_getinteger(vm, 3, &iTag);

	const SQChar *tagRet = BASS_ChannelGetTags(iStream, iTag);
	sq_pushstring(vm, tagRet, -1);

	return 1;
}

SQInteger sq_bass_channel_flags(HSQUIRRELVM vm)
{
	SQInteger iStream;
	SQInteger flags;
	SQInteger mask;
	
	sq_getinteger(vm, 2, &iStream);
	sq_getinteger(vm, 3, &flags);
	sq_getinteger(vm, 4, &mask);

	SQInteger resultFlags = BASS_ChannelFlags(iStream, flags, mask);
	sq_pushinteger(vm, resultFlags);

	return 1;
}

SQInteger sq_bass_error_get_code(HSQUIRRELVM vm)
{
	sq_pushinteger(vm, BASS_ErrorGetCode());

	return 1;
}

SQInteger sq_bass_init(HSQUIRRELVM vm)
{
	SQInteger device;
	SQInteger freq;
	SQInteger flags;

	sq_getinteger(vm, 2, &device);
	sq_getinteger(vm, 3, &freq);
	sq_getinteger(vm, 4, &flags);

	if (BASS_Init(device, freq, flags, NULL, NULL))
	{
		sq_pushbool(vm, true);
	}
	else
	{
		sq_pushbool(vm, false);
	}

	return 1;
}