#pragma once

SQInteger sq_bass_stream_create_file(HSQUIRRELVM vm);
SQInteger sq_bass_stream_create_url(HSQUIRRELVM vm);
SQInteger sq_bass_stream_free(HSQUIRRELVM vm);
SQInteger sq_bass_get_volume(HSQUIRRELVM vm);
SQInteger sq_bass_set_volume(HSQUIRRELVM vm);
SQInteger sq_bass_channel_stop(HSQUIRRELVM vm);
SQInteger sq_bass_channel_play(HSQUIRRELVM vm);
SQInteger sq_bass_channel_pause(HSQUIRRELVM vm);
SQInteger sq_bass_channel_get_tags(HSQUIRRELVM vm);
SQInteger sq_bass_channel_flags(HSQUIRRELVM vm);
SQInteger sq_bass_error_get_code(HSQUIRRELVM vm);
SQInteger sq_bass_init(HSQUIRRELVM vm);