#include "SqFunction.h"
#include <bass.h>
#include <sqrat.h>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	Sqrat::RootTable roottable(vm);

	roottable.SquirrelFunc("BASS_Init", sq_bass_init, 4, ".iii");
	roottable.SquirrelFunc("BASS_StreamCreateFile", sq_bass_stream_create_file, 5, ".siii");
	roottable.SquirrelFunc("BASS_StreamCreateURL", sq_bass_stream_create_url, 4, ".sii");
	roottable.SquirrelFunc("BASS_StreamFree", sq_bass_stream_free, 2, ".i");
	roottable.SquirrelFunc("BASS_GetVolume", sq_bass_get_volume, 2, ".i");
	roottable.SquirrelFunc("BASS_SetVolume", sq_bass_set_volume, 3, ".ii");
	roottable.SquirrelFunc("BASS_ChannelStop", sq_bass_channel_stop, 2, ".i");
	roottable.SquirrelFunc("BASS_ChannelPlay", sq_bass_channel_play, 3, ".ib");
	roottable.SquirrelFunc("BASS_ChannelPause", sq_bass_channel_pause, 2, ".i");
	roottable.SquirrelFunc("BASS_ChannelGetTags", sq_bass_channel_get_tags, 3, ".ii");
	roottable.SquirrelFunc("BASS_ChannelFlags", sq_bass_channel_flags, 4, ".iii");
	roottable.SquirrelFunc("BASS_ErrorGetCode", sq_bass_error_get_code, 1);

	Sqrat::ConstTable consttable(vm);

	consttable.Const("BASS_SAMPLE_LOOP", BASS_SAMPLE_LOOP);

	consttable.Const("BASS_TAG_ID3", BASS_TAG_ID3);
	consttable.Const("BASS_TAG_ID3V2", BASS_TAG_ID3V2);
	consttable.Const("BASS_TAG_OGG", BASS_TAG_OGG);
	consttable.Const("BASS_TAG_HTTP", BASS_TAG_HTTP);
	consttable.Const("BASS_TAG_ICY", BASS_TAG_ICY);
	consttable.Const("BASS_TAG_META", BASS_TAG_META);
	consttable.Const("BASS_TAG_APE", BASS_TAG_APE);
	consttable.Const("BASS_TAG_MP4", BASS_TAG_MP4);
	consttable.Const("BASS_TAG_VENDOR", BASS_TAG_VENDOR);
	consttable.Const("BASS_TAG_LYRICS3", BASS_TAG_LYRICS3);
	consttable.Const("BASS_TAG_CA_CODEC", BASS_TAG_CA_CODEC);
	consttable.Const("BASS_TAG_MF", BASS_TAG_MF);
	consttable.Const("BASS_TAG_WAVEFORMAT", BASS_TAG_WAVEFORMAT);
	consttable.Const("BASS_TAG_RIFF_INFO", BASS_TAG_RIFF_INFO);
	consttable.Const("BASS_TAG_RIFF_BEXT", BASS_TAG_RIFF_BEXT);
	consttable.Const("BASS_TAG_RIFF_CART", BASS_TAG_RIFF_CART);
	consttable.Const("BASS_TAG_RIFF_DISP", BASS_TAG_RIFF_DISP);
	consttable.Const("BASS_TAG_APE_BINARY", BASS_TAG_APE_BINARY);
	consttable.Const("BASS_TAG_MUSIC_NAME", BASS_TAG_MUSIC_NAME);
	consttable.Const("BASS_TAG_MUSIC_MESSAGE", BASS_TAG_MUSIC_MESSAGE);
	consttable.Const("BASS_TAG_MUSIC_ORDERS", BASS_TAG_MUSIC_ORDERS);
	consttable.Const("BASS_TAG_MUSIC_INST", BASS_TAG_MUSIC_INST);
	consttable.Const("BASS_TAG_MUSIC_SAMPLE", BASS_TAG_MUSIC_SAMPLE);

	consttable.Const("BASS_OK", BASS_OK);
	consttable.Const("BASS_ERROR_MEM", BASS_ERROR_MEM);
	consttable.Const("BASS_ERROR_FILEOPEN", BASS_ERROR_FILEOPEN);
	consttable.Const("BASS_ERROR_DRIVER", BASS_ERROR_DRIVER);
	consttable.Const("BASS_ERROR_BUFLOST", BASS_ERROR_BUFLOST);
	consttable.Const("BASS_ERROR_HANDLE", BASS_ERROR_HANDLE);
	consttable.Const("BASS_ERROR_FORMAT", BASS_ERROR_FORMAT);
	consttable.Const("BASS_ERROR_POSITION", BASS_ERROR_POSITION);
	consttable.Const("BASS_ERROR_INIT", BASS_ERROR_INIT);
	consttable.Const("BASS_ERROR_START", BASS_ERROR_START);
	consttable.Const("BASS_ERROR_SSL", BASS_ERROR_SSL);
	consttable.Const("BASS_ERROR_ALREADY", BASS_ERROR_ALREADY);
	consttable.Const("BASS_ERROR_NOCHAN", BASS_ERROR_NOCHAN);
	consttable.Const("BASS_ERROR_ILLTYPE", BASS_ERROR_ILLTYPE);
	consttable.Const("BASS_ERROR_ILLPARAM", BASS_ERROR_ILLPARAM);
	consttable.Const("BASS_ERROR_NO3D", BASS_ERROR_NO3D);
	consttable.Const("BASS_ERROR_NOEAX", BASS_ERROR_NOEAX);
	consttable.Const("BASS_ERROR_DEVICE", BASS_ERROR_DEVICE);
	consttable.Const("BASS_ERROR_NOPLAY", BASS_ERROR_NOPLAY);
	consttable.Const("BASS_ERROR_FREQ", BASS_ERROR_FREQ);
	consttable.Const("BASS_ERROR_NOTFILE", BASS_ERROR_NOTFILE);
	consttable.Const("BASS_ERROR_NOHW", BASS_ERROR_NOHW);
	consttable.Const("BASS_ERROR_EMPTY", BASS_ERROR_EMPTY);
	consttable.Const("BASS_ERROR_NONET", BASS_ERROR_NONET);
	consttable.Const("BASS_ERROR_CREATE", BASS_ERROR_CREATE);
	consttable.Const("BASS_ERROR_NOFX", BASS_ERROR_NOFX);
	consttable.Const("BASS_ERROR_NOTAVAIL", BASS_ERROR_NOTAVAIL);
	consttable.Const("BASS_ERROR_DECODE", BASS_ERROR_DECODE);
	consttable.Const("BASS_ERROR_DX", BASS_ERROR_DX);
	consttable.Const("BASS_ERROR_TIMEOUT", BASS_ERROR_TIMEOUT);
	consttable.Const("BASS_ERROR_FILEFORM", BASS_ERROR_FILEFORM);
	consttable.Const("BASS_ERROR_SPEAKER", BASS_ERROR_SPEAKER);
	consttable.Const("BASS_ERROR_VERSION", BASS_ERROR_VERSION);
	consttable.Const("BASS_ERROR_CODEC", BASS_ERROR_CODEC);
	consttable.Const("BASS_ERROR_ENDED", BASS_ERROR_ENDED);
	consttable.Const("BASS_ERROR_BUSY", BASS_ERROR_BUSY);
	consttable.Const("BASS_ERROR_UNKNOWN", BASS_ERROR_UNKNOWN);

	return SQ_OK;
}
